﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climber : MonoBehaviour
{
    // initial facing
    private Direction facing = Direction.Left;
    // current block
    private Block atBlock = null;
	// next block
	private Block toBlock = null;
	// moving speed
	private float moveXPerSec = 0.0f;
	private float moveYPerSec = 0.0f;
	// total jump
	private int totalJumps = 0;

    public void AssignAtBlock(Block at)
    {
        atBlock = at;
        // move display to block
        PositionAtBlock();
    }
	
	public bool HasLanded()
	{
		return !atBlock.IsEmpty();
	}

    private void PositionAtBlock()
    {
		this.transform.position = atBlock.transform.position;
		this.transform.SetParent(atBlock.transform);
    }
	
	public void Land()
	{
		// snap to next block
		if (toBlock != null)
		{
			atBlock = toBlock;
			PositionAtBlock();
			++totalJumps;
		}
	}

    public void Jump(float interval)
	{
		// get next block
		toBlock = atBlock.GetLinkedBlock(facing);

		if (toBlock != null)
		{
			// two positions
			Vector3 currPos = atBlock.transform.position;
			Vector3 nextPos = toBlock.transform.position;
			// calculate speed
			moveXPerSec = (nextPos.x - currPos.x) * (1 / interval);
			moveYPerSec = (nextPos.y - currPos.y) * (1 / interval);
		}
		else
		{
			moveXPerSec = 0.0f;
			moveYPerSec = 0.0f;
		}
    }
	
	public void Turn()
	{
		if (facing == Direction.Left)
		{
			facing = Direction.Right;
		}
		else
		{
			facing = Direction.Left;
		}
	}
	
	public int GetTotalJumps()
	{
		return totalJumps;
	}

	private void Update()
	{
		// move to target
		Vector3 pos = this.gameObject.transform.position;
		pos.x += moveXPerSec * Time.deltaTime;
		pos.y += moveYPerSec * Time.deltaTime;
		this.gameObject.transform.position = pos;
	}

    public Block GetAtBlock()
    {
        return atBlock;
    }

    public Block GetToBlock()
    {
        return toBlock;
    }

    public Direction GetFacing()
    {
        return facing;
    }

    public void SetFacing(Direction direction)
    {
        facing = direction;
    }
}

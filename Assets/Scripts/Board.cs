﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    // size of the board
    public int maxColumns = 6;
    public int maxRows = 22;
    // chance of the holes
    public float initialChanceOfHoles = 0.1f;
    // link block prefab
    public Block blockPrefab;
	// starting row
	private int startingRowIndex = 10;
    private Block lastPathBlock = null;

    // board generation
	private int lastRowGenerated = -1;
    private List<Block[]> board = null;
    private int columnSizePerRow = -1;
    // size
    private float blockWidthInWorld = 1.41f;
    private float blockHeightInWorld = 1.21f;

    // Use this for initialization
    void Start()
    {
		// validity check
		if (maxRows < 2 || maxRows % 2 == 1 || maxColumns < 2 || startingRowIndex % 2 == 1)
		{
			Debug.LogError("Invalid max Row/Column");
			return;
		}
		
        // list size is full column + 1
        columnSizePerRow = maxColumns + 1;

        // creates board
        board = new List<Block[]>();
        for (int i = 0; i < maxRows; ++i)
        {
            Block[] blocks = new Block[columnSizePerRow];
            // creates blocks
            for (int j = 0; j < columnSizePerRow; ++j)
            {
                Block blockCreated = Instantiate<Block>(blockPrefab);
                blockCreated.name = "Block_" + i + "_" + j;
                blocks[j] = blockCreated;
            }
            // add to row
            board.Add(blocks);
        }

        // display initial board
        for (int i = 0; i < maxRows; ++i)
        {
            // get a row
            Block[] blocks = board[i];

            // board orign
            float startX = 0.0f;
            float startY = this.gameObject.transform.position.y;

            // align blocks at centre
            startX = ((columnSizePerRow - 1) * blockWidthInWorld) * -0.5f;
			if (IsFullColumn(i))
                startX += blockWidthInWorld * 0.5f;

            // place blocks
            for (int j = 0; j < columnSizePerRow; ++j)
            {
                // set position
                Vector3 pos = blocks[j].transform.position;
                pos.x = startX + j * blockWidthInWorld;
                pos.y = startY + i * blockHeightInWorld;
                blocks[j].transform.position = pos;
                // move to under this gameobject
                blocks[j].transform.SetParent(this.gameObject.transform);
            }
        }

        // links blocks, build looped board, set edges
        for (int i = 0; i < maxRows; ++i)
        {
            // find previous row, one row below
            int prevI = i - 1;
            if (prevI < 0)
                prevI = maxRows - 1;
            // blocks in current row
            Block[] currRow = board[i];
            // blocks in previous row
            Block[] prevRow = board[prevI];

            // link each blocks in a current row to prev row
            for (int j = 0; j < columnSizePerRow; ++j)
            {
                int leftIndex = j;
                int rightIndex = j;
				
				// adjust based on column type
				if (IsFullColumn(i))
				{
					rightIndex += 1;
                    // set edges
                    if (j == columnSizePerRow - 1)
                    {
                        currRow[j].SetAsEdge();
                    }
				}
				else
				{
					leftIndex -= 1;
                    // set edges
                    if (j == 0 || j == columnSizePerRow - 1)
                    {
                        currRow[j].SetAsEdge();
                    }
				}

                // fully assign neighbours
                if (leftIndex >= 0)
                {
                    prevRow[leftIndex].SetLinkedBlock(Direction.Right, currRow[j]);
                }
                if (rightIndex < columnSizePerRow)
                {
                    prevRow[rightIndex].SetLinkedBlock(Direction.Left, currRow[j]);
                }
            }
        }

		// generate empty row manually
		for (int i = 0; i < startingRowIndex; ++i)
		{
			GenerateRow(0, 0.0f);
		}

        // generate first row manually
		GenerateRow(1, 0.0f);
		lastPathBlock = GetStartingBlock();		// start path after first block
        lastPathBlock.SetAsPath();

        // initialize boards, substract first one manually
		for (int i = startingRowIndex + 1; i < maxRows; ++i)
        {
            GenerateNextRow(0.0f);
        }
    }
	
	private bool IsFullColumn(int columnCount)
	{
		return (columnCount % 2 == 1);
	}

    private void GenerateNextRow(float chanceOfHoles)
    {
        // toggle full columns and full - 1
		int cols = IsFullColumn(lastRowGenerated) ? maxColumns - 1 : maxColumns;
        // generate new one
        GenerateRow(cols, chanceOfHoles);
    }

    private void GenerateRow(int numBlocks, float chanceOfHoles)
	{
		// increase index, indexing last row
		++lastRowGenerated;
		if (lastRowGenerated >= maxRows)
		{
			lastRowGenerated = 0;
		}
		
        // calculate start index, as middle as possible in the row
        int startIndex = Mathf.FloorToInt((columnSizePerRow - numBlocks) * 0.5f);

        // find target row to generate
        Block[] blocks = board[lastRowGenerated];
        for (int i = 0; i < columnSizePerRow; ++i)
        {
            // randomize empty block
            if (i >= startIndex && i < numBlocks + startIndex && Random.value >= chanceOfHoles)
            {
                // find path
                blocks[i].SetAsNormalBlock();
            }
            else
            {
                // empty block
                blocks[i].SetAsEmptyBlock();
            }
        }

        if (lastPathBlock != null)
            PathGuarantor();
    }

    private void PathGuarantor()
    {
        Block leftBlock = lastPathBlock.GetLinkedBlock(Direction.Left);
        Block rightBlock = lastPathBlock.GetLinkedBlock(Direction.Right);
		// add randomness
		Block firstBlock = null;
		Block secondBlock = null;
		if (Random.value < 0.5f)
		{
			firstBlock = leftBlock;
			secondBlock = rightBlock;
		}
		else
		{
			firstBlock = rightBlock;
			secondBlock = leftBlock;
		}

        // assign any open block
		if (firstBlock.IsEmpty() == false)
        {
			firstBlock.SetAsPath();
			lastPathBlock = firstBlock;
        }
		else if (secondBlock.IsEmpty() == false)
        {
			secondBlock.SetAsPath();
			lastPathBlock = secondBlock;
        }
        else
        {
            // or gaurantee path
			if (firstBlock.IsEdge() == false && Random.value < 0.5f)
            {
				firstBlock.SetAsNormalBlock();
				firstBlock.SetAsPath();
				lastPathBlock = firstBlock;
            }
            else
            {
				secondBlock.SetAsNormalBlock();
				secondBlock.SetAsPath();
				lastPathBlock = secondBlock;
            }
        }
    }

    public void PrepareNewRow()
    {
		// get position of last row + calculate new height
		float newHeight = board[lastRowGenerated][0].transform.position.y + blockHeightInWorld;
		
		// new line
        GenerateNextRow(initialChanceOfHoles);

		// new row
		Block[] blocks = board[lastRowGenerated];
		// repos new row
		for (int i = 0; i < blocks.Length; ++i)
		{
			Vector3 pos = blocks[i].transform.position;
			pos.y = newHeight;
			blocks[i].transform.position = pos;
		}
    }

    public Block GetStartingBlock()
    {
        // get centre block of first row, only valid at beginning
		return board[startingRowIndex][Mathf.FloorToInt((columnSizePerRow - 1) * 0.5f)];
    }
}

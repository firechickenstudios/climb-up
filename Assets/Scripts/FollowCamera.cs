﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// constant following target camear, only follow y position
public class FollowCamera : MonoBehaviour
{
	// follow target
    private Climber target;
	// save offset of initial pos
	private Vector3 targetOriginPos;

    public void SetOffset(Vector3 offset)
    {
        targetOriginPos = offset;
    }
	
    public void SetTarget(Climber target)
	{
		this.target = target;
	}

	// Update is called once per frame
	void Update()
	{
		if (target == null)
		{
			return;
		}

		// keep follow target
		Vector3 pos = this.transform.position;
		pos.y = target.transform.position.y - targetOriginPos.y;
		this.transform.position = pos;
	}
}

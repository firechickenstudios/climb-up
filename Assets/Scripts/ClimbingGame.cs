﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingGame : MonoBehaviour
{
    // interval in second format
    public float jumpingInterval = 0.3f;
    private float timePast = 0.0f;
    public int maxClimbers = 5;

    // link prefab of climber
    public Climber climberPrefab;       // TODO resource factory includes blocks
    private List<Climber> climbers;

	// controller
	public Controller controller;

    // game baord
    public Board board;

	// link camera controller
	public FollowCamera cameraController;

	// gamee status
	private bool running = false;

    private int testInt = 1;

    // Use this for initialization
    void Start()
    {
        // climber group
        climbers = new List<Climber>();

        AddClimber(board.GetStartingBlock());

		// set target to camera
        cameraController.SetTarget(GetLeadClimber());
        cameraController.SetOffset(GetLeadClimber().transform.position);

        // set initial one as a leader
        controller.AssignLeadClimber(GetLeadClimber());

		// start of the game
		running = true;
    }

    private void AddClimber(Block startingBlock)
    {
        // create climber, player
        Climber climber = Instantiate<Climber>(climberPrefab);
        climber.name = "Climber_" + climbers.Count;
        climbers.Add(climber);

        // assign to starting block
        climber.AssignAtBlock(startingBlock);
    }

    private void RemoveClimber(Climber climber)
    {
        // remove from the list
        climbers.Remove(climber);
        // reset camera
        if (GetLeadClimber() != null)
        {
            cameraController.SetTarget(GetLeadClimber());
        }
        // delete instance
        Destroy(climber);
    }

    private Climber GetLeadClimber()
    {
        if (climbers.Count <= 0)
            return null;
        return climbers[0];
    }

    private Climber GetLastClimber()
    {
        return climbers[climbers.Count - 1];
    }

    // Update is called once per frame
    void Update()
    {
		// only run when it is active
		if (running == false)
		{
			return;
		}
		
		// TODO coroutine
        timePast += Time.deltaTime;
		if (timePast < jumpingInterval)
		{
            return;
        }

        Climber dead = null;

        for (int i = 0; i < climbers.Count; ++i)
        {
            // landing
            climbers[i].Land();

            // check liveness, has to be after call StepUp
            if (climbers[i].HasLanded() == false)
            {
                // remove climber
                dead = climbers[i];
                // fill block
                if (dead.GetAtBlock().IsEdge() == false)
                {
                    dead.GetAtBlock().SetAsNormalBlock();
                }
            }
        }

        if (dead != null)
        {
            RemoveClimber(dead);
        }

        // follow dude ahead (descend order)
        for (int i = climbers.Count - 1; i >= 0; --i)
        {
            if (i > 0)
            {
                if (climbers[i].GetFacing() != climbers[i - 1].GetFacing())
                {
                    climbers[i].Turn();
                }
            }
            else
            {
                // TODO GetLeadClimber
                // read conroller
                if (controller.ReadTapped())
                {
                    climbers[i].Turn();
                }
            }
        }

        for (int i = 0; i < climbers.Count; ++i)
        {
            // step up
            climbers[i].Jump(jumpingInterval);
        }

        // item logic between land and correcting facing
        if (testInt < maxClimbers)
        {
            // test add dude item
            Climber lastOne = GetLastClimber();
            AddClimber(lastOne.GetAtBlock());
            Climber climberAdded = GetLastClimber();
            climberAdded.SetFacing(lastOne.GetFacing());
            ++testInt;
        }

        if (climbers.Count <= 0)
        {
            Debug.Log("Game Over");
            running = false;
            return;
        }

        // generate new line in board
        board.PrepareNewRow();

        timePast = 0.0f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// block is basically use customized linked list structure
public class Block : MonoBehaviour
{
    public GameObject displayObject;

    // linked blocks
    private Block leftBlock = null;
    private Block rightBlock = null;

    // block type can be normal and empty
    private bool isEmpty = true;
    private bool isEdge = false;
    // guaranteed path
    public bool isPathBlock = false;

    // get linked block from direction
    public Block GetLinkedBlock(Direction direction)
    {
        if (direction == Direction.Left)
        {
            return leftBlock;
        }
        else if (direction == Direction.Right)
        {
            return rightBlock;
        }

        Debug.LogError("Not supported direction");
        return null;
    }

    // assign block
    public void SetLinkedBlock(Direction direction, Block block)
    {
        if (direction == Direction.Left)
        {
            leftBlock = block;
        }
        else if (direction == Direction.Right)
        {
            rightBlock = block;
        }
    }

    // set block type
    public void SetAsNormalBlock()
    {
        // reset item flag and item if exist
        isEmpty = false;
        // reset path flag
        isPathBlock = false;
        // update display
        displayObject.SetActive(true);
    }

    public void SetAsEmptyBlock()
    {
        // reset item flag and item if exist
		isEmpty = true;
        // reset path flag
        isPathBlock = false;
        // update display
        displayObject.SetActive(false);
    }

    // boolean value for the block type
    public bool IsEmpty()
    {
        return isEmpty;
    }

    // boolean flag whether block has item or not
    public bool HasItem()
    {
        return false;
    }

    public bool IsPath()
    {
        return isPathBlock;
    }

    public void SetAsPath()
    {
        isPathBlock = true;
    }

    public void SetAsEdge()
    {
        // permanent setting
        isEdge = true;
    }

    public bool IsEdge()
    {
        return isEdge;
    }
}

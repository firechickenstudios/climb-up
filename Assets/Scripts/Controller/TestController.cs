﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : Controller
{
	public override bool ReadTapped()
	{
        Direction direction = leadClimber.GetFacing();
		Block at = leadClimber.GetAtBlock();

        if (at != null)
        {
            Block left = at.GetLinkedBlock(Direction.Left);
            Block right = at.GetLinkedBlock(Direction.Right);

            if (direction == Direction.Left && left.IsPath() == false)
            {
                return true;
            }
            else if (direction == Direction.Right && right.IsPath() == false)
            {
                return true;
            }
        }

        return false;

        // follow available ones
        /*Block willLandAt = leadClimber.GetToBlock();
        if (willLandAt != null)
        {
            if (willLandAt.GetLinkedBlock(direction).IsEmpty() == false)
            {
                return false;
            }
        }

        return true;*/
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
	protected bool tapped = false;

    protected Climber leadClimber = null;

    // set lead of the climber group
    public void AssignLeadClimber(Climber climber)
    {
        leadClimber = climber;
    }
	
    // read decision
	virtual public bool ReadTapped()
	{
		bool ret = tapped;
		tapped = false;
		return ret;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapController : Controller
{
	// Update is called once per frame
	void Update()
	{
		// turn when hit
		if (tapped == false && Input.GetMouseButtonDown(0))
		{
			tapped = true;
		}
	}
}
